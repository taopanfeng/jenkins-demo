FROM adoptopenjdk/openjdk8:centos

LABEL name=TaoPanfeng

COPY target/*.jar /app.jar

CMD java -jar /app.jar