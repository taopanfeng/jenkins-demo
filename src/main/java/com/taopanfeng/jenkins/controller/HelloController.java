package com.taopanfeng.jenkins.controller;


import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateTime;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * 简单 rest 接口测试
 *
 * @author 陶攀峰
 * @version 1.0
 * @date 2021-03-05 19:28
 */
@RestController
public class HelloController {

    @GetMapping("hello")
    public Map<String, Object> hello() throws Exception {
        HashMap<String, Object> map = CollectionUtil.newHashMap();
        map.put("currentTime", DateTime.now().toString());
        map.put("UUID", UUID.randomUUID().toString());
        map.put("host", InetAddress.getLocalHost().toString());
        return map;
    }
}
